
# install docker
sudo apt-get install -y apparmor # do this first so daemon will start
wget -qO- https://get.docker.com/ | sh

# can check logs with /var/log/upstart/docker.log if it doesn't start

# get rid of warning about cgroup swap limit
#sudo nano /etc/default/grub
#	add "cgroup_enable=memory swapaccount=1" to GRUB_CMDLINE_LINUX
#sudo update-grub
#sudo reboot

user=$USER
sudo usermod -aG docker $user # requires logout/login

# get direct-connect pipework
git clone https://github.com/Rakurai/pipework.git pipework
sudo ln pipework/pipework /usr/local/bin/pipework



