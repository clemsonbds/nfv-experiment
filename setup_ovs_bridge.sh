#!/bin/bash

port=$1
bridge=br1
ip=10.10.1.3

sudo ovs-vsctl add-br $bridge
sudo ovs-vsctl add-port $bridge $port
sudo ifconfig $port 0
sudo ifconfig $bridge $ip

