#!/bin/bash

target=$1
iterations=$2
length=$3
request_size=$4
# do we care about local or remote socket buffer size?  maybe experiment to get lowest results first

[ -z "$target" ] && {
  echo "usage: $0 target iterations length request_size"
  exit
}

[ -z "$iterations" ] && iterations=1
[ -z "$length" ] && length=10
[ -z "$request_size" ] && request_size=64

request_size=$(($request_size-42))

[ "$iterations" -gt 1 ] && iter_str="-i $iterations"

netperf -H $target -t UDP_STREAM $iter_str -l $length -- -m ${request_size}
