#!/bin/bash

DPDK_NUM_HUGEPAGES=8
DPDK_HUGE_DIR=/mnt/huge
ivshmem=n
vfio=n

DPDK_DIR=${HOME}/dpdk
RTE_SDK=${DPDK_DIR}
RTE_TARGET=x86_64-native-linuxapp-gcc
[ "$ivshmem" == "y" ] && RTE_TARGET=x86_64-ivshmem-linuxapp-gcc
DPDK_BUILD=${DPDK_DIR}/${RTE_TARGET}

# before starting

# Add env variables setting to .profile file so that they are set at each login
echo "export DPDK_NUM_HUGEPAGES=$DPDK_NUM_HUGEPAGES" >> ${HOME}/.profile
echo "export DPDK_HUGE_DIR=$DPDK_HUGE_DIR" >> ${HOME}/.profile
echo "export DPDK_DIR=$DPDK_DIR" >> ${HOME}/.profile
echo "export RTE_SDK=$DPDK_DIR" >> ${HOME}/.profile
echo "export RTE_TARGET=$RTE_TARGET" >> ${HOME}/.profile
echo "export DPDK_BUILD=$DPDK_BUILD" >> ${HOME}/.profile
source ${HOME}/.profile

# upgrade kernel
wget http://kernel.ubuntu.com/~kernel-ppa/mainline/v4.0.4-wily/linux-headers-4.0.4-040004_4.0.4-040004.201505171336_all.deb
wget http://kernel.ubuntu.com/~kernel-ppa/mainline/v4.0.4-wily/linux-headers-4.0.4-040004-generic_4.0.4-040004.201505171336_amd64.deb
wget http://kernel.ubuntu.com/~kernel-ppa/mainline/v4.0.4-wily/linux-image-4.0.4-040004-generic_4.0.4-040004.201505171336_amd64.deb
sudo dpkg -i linux-headers-4.0.4*.deb linux-image-4.0.4*.deb
# sudo reboot

# change ulimit
sudo bash -c 'echo "* soft memlock -1" >> /etc/security/limits.conf'
sudo bash -c 'echo "* hard memlock -1" >> /etc/security/limits.conf'

# Configure hugepages (1GB, must be done before boot)
# You can later check if this change was successful with "cat /proc/meminfo"
kernel_flags=`printf "default_hugepagesz=1GB hugepagesz=1G hugepages=%d iommu=pt intel_iommu=on" $DPDK_NUM_HUGEPAGES`
sudo sed -i 's/GRUB_CMDLINE_LINUX_DEFAULT=""/GRUB_CMDLINE_LINUX_DEFAULT="'"${kernel_flags}"'"/' /etc/default/grub
sudo update-grub
sudo mkdir $DPDK_HUGE_DIR
fstab_line=`printf "nodev %s hugetlbfs pagesize=1GB 0 0" $DPDK_HUGE_DIR`
sudo bash -c 'echo "'"$fstab_line"'" >> /etc/fstab'
# sudo reboot
