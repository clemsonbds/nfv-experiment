#!/bin/sh

#Source : https://github.com/openvswitch/ovs/blob/master/INSTALL.DPDK.md

export DPDK_NUM_HUGEPAGES=8
export DPDK_HUGE_DIR=/mnt/huge_1GB
export ivshmem=n
export vfio=n

DPDK_DIR=${HOME}/dpdk
RTE_SDK=${DPDK_DIR}
RTE_TARGET=x86_64-native-linuxapp-gcc
[ "$ivshmem" == "y" ] && RTE_TARGET=x86_64-ivshmem-linuxapp-gcc
DPDK_BUILD=${DPDK_DIR}/${RTE_TARGET}

# before starting

# Add env variables setting to .profile file so that they are set at each login
echo "export DPDK_NUM_HUGEPAGES=$DPDK_NUM_HUGEPAGES" >> ${HOME}/.profile
echo "export DPDK_HUGE_DIR=$DPDK_HUGE_DIR" >> ${HOME}/.profile
echo "export DPDK_DIR=$DPDK_DIR" >> ${HOME}/.profile
echo "export RTE_SDK=$DPDK_DIR" >> ${HOME}/.profile
echo "export RTE_TARGET=$RTE_TARGET" >> ${HOME}/.profile
echo "export DPDK_BUILD=$DPDK_BUILD" >> ${HOME}/.profile
source ${HOME}/.profile

# upgrade kernel
wget http://kernel.ubuntu.com/~kernel-ppa/mainline/v4.0.4-wily/linux-headers-4.0.4-040004_4.0.4-040004.201505171336_all.deb
wget http://kernel.ubuntu.com/~kernel-ppa/mainline/v4.0.4-wily/linux-headers-4.0.4-040004-generic_4.0.4-040004.201505171336_amd64.deb
wget http://kernel.ubuntu.com/~kernel-ppa/mainline/v4.0.4-wily/linux-image-4.0.4-040004-generic_4.0.4-040004.201505171336_amd64.deb
sudo dpkg -i linux-headers-4.0.4*.deb linux-image-4.0.4*.deb
# sudo reboot

# change ulimit
export memlock_kbytes=$((1048576*${DPDK_NUM_HUGEPAGES}))
sudo bash -c 'echo "* soft memlock $memlock_kbytes" >> /etc/security/limits.conf'
sudo bash -c 'echo "* hard memlock $memlock_kbytes" >> /etc/security/limits.conf'

# Configure hugepages (1GB, must be done before boot)
# You can later check if this change was successful with "cat /proc/meminfo"
kernel_flags="default_hugepagesz=1GB hugepagesz=1G hugepages=$DPDK_NUM_HUGEPAGES iommu=pt intel_iommu=on"
sudo sed -i 's/GRUB_CMDLINE_LINUX_DEFAULT=""/GRUB_CMDLINE_LINUX_DEFAULT=${kernel_flags}/' /etc/default/grub
sudo update-grub
sudo mkdir $DPDK_HUGE_DIR
sudo bash -c 'echo "nodev $DPDK_HUGE_DIR hugetlbfs pagesize=1GB 0 0" >> /etc/fstab'
# sudo reboot

# or configure hugepages (2048K pages, after boot)
# configure once, needs to be done as soon as possible after reboot to avoid getting fragmented memory (does not persist)
# HUGEPAGE_MOUNT=/mnt/huge
# echo 1024 | sudo tee /sys/kernel/mm/hugepages/hugepages-2048kB/nr_hugepages
# sudo mkdir ${HUGEPAGE_MOUNT}
# sudo mount -t hugetlbfs nodev ${HUGEPAGE_MOUNT}

sudo apt-get update
sudo apt-get -y -q install git clang doxygen hugepages build-essential linux-headers-`uname -r`
sudo apt-get -y -q install fuse libfuse-dev

# Get code from Git repo
git clone http://dpdk.org/git/dpdk

cd dpdk

# change the config for ovs with dpdk
sed -i s/CONFIG_RTE_BUILD_COMBINE_LIBS=n/CONFIG_RTE_BUILD_COMBINE_LIBS=y/ config/common_linuxapp
sed -i s/CONFIG_RTE_LIBRTE_VHOST=n/CONFIG_RTE_LIBRTE_VHOST=y/ config/common_linuxapp
sed -i s/CONFIG_RTE_LIBRTE_VHOST_USER=y/CONFIG_RTE_LIBRTE_VHOST_USER=n/ config/common_linuxapp

#Build


# We need to do this to make the examples compile, not sure why.
ln -s ${RTE_SDK}/${RTE_TARGET} ${RTE_SDK}/build

make install T=${RTE_TARGET}

#adjust limits for VFIO
printf "* soft memlock 8388608" >> /etc/security/limits.conf
printf "* hard memlock 8388608" >> /etc/security/limits.conf

# Install kernel modules
if [ "$vfio" == "y" ]; then
  sudo modprobe vfio-pci
else
  sudo modprobe uio
  sudo insmod ${RTE_SDK}/build/kmod/igb_uio.ko
fi

cd ..

# install openvswitch
git clone https://github.com/openvswitch/ovs.git

cd ovs
./boot.sh
CFLAGS="-g -O2 -Wno-cast-align" ./configure --with-dpdk=$DPDK_BUILD
sudo make install CFLAGS='-O3 -march=native'

# Bind secondary network adapter
# Note: NIC setup does not persist across reboots and also you need to specify the ethnumber
sudo ifconfig eth1 down
sudo ${RTE_SDK}/tools/dpdk_nic_bind.py --bind=igb_uio eth1

# start OVS
sudo mkdir -p /usr/local/etc/openvswitch
sudo mkdir -p /usr/local/var/run/openvswitch
sudo rm /usr/local/etc/openvswitch/conf.db
sudo ovsdb-tool create /usr/local/etc/openvswitch/conf.db  \
       /usr/local/share/openvswitch/vswitch.ovsschema
sudo ovsdb-server --remote=punix:/usr/local/var/run/openvswitch/db.sock \
    --remote=db:Open_vSwitch,Open_vSwitch,manager_options \
    --private-key=db:Open_vSwitch,SSL,private_key \
    --certificate=Open_vSwitch,SSL,certificate \
    --bootstrap-ca-cert=db:Open_vSwitch,SSL,ca_cert --pidfile --detach
sudo ovs-vsctl --no-wait --log-file=/users/rakurai/vsctl.log init
export DB_SOCK=/usr/local/var/run/openvswitch/db.sock
sudo rm /dev/vhost-net
common_options="--dpdk -c 0xff -n 4 --huge-dir=${DPDK_HUGE_DIR}"
if [ "$ivshmem" == "y" ]; then
  sudo ovs-vswitchd $common_options --socket-mem 1024,0 \
      -- unix:$DB_SOCK --pidfile --detach --log-file=/users/rakurai/vswitchd.log
else
  sudo ovs-vswitchd $common_options \
      -- unix:$DB_SOCK --pidfile --detach --log-file=/users/rakurai/vswitchd.log
fi
