#!/bin/bash

docker kill $(docker ps -q)

for ns in `ip netns`; do
  sudo ovs-vsctl del-port veth1pl$ns
  sudo ip netns del $ns
done

sudo ovs-ofctl del-flows br1
