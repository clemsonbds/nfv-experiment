#!/usr/bin/python

import sys

sendfile = sys.argv[1]
recvfile = sys.argv[2]

print sendfile, recvfile

sends = {}

for line in open(sendfile):
	columns = line[:-1].split('\t')
	sends[columns[1]] = columns[0]

for line in open(recvfile):
	columns = line[:-1].split('\t')
	num = columns[1]
	if num in sends:
		recv = columns[0]
		send = sends[num]
		print num, send, recv
