#!/bin/bash

host=$1
prefix=$2
dir=results

spps_file=$dir/conc_stream_${prefix}_spps.txt
rpps_file=$dir/conc_stream_${prefix}_rpps.txt
rm $spps_file
rm $rpps_file

for size in 64 128 256 512 1024 1280 1514; do
  spps_line="$size "
  rpps_line="$size "
  for ncpus in 1 2 4 8 16; do
    output=`./perf_tp_conc.sh $host $ncpus $size`
    echo $output
    comp=`echo "$output" | grep cmpl | awk '{print $2}'`
    spps=`echo "$output" | grep spps | awk '{print $2}'`
    rpps=`echo "$output" | grep rpps | awk '{print $2}'`

    [ "$comp" -ne "$ncpus" ] && {
      spps="ERROR"
      rpps="ERROR"
    }

    spps_line="$spps_line$spps "
    rpps_line="$rpps_line$rpps "
  done

  echo $spps_line >> $spps_file
  echo $rpps_line >> $rpps_file
done
