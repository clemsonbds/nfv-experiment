#!/bin/bash

iface=$1

[ -z "$iface" ] && echo "Need an interface." && exit

ip=$(ip addr | awk '/inet/ && /'$iface'/{sub(/\/.*$/,"",$2); print $2}')
echo $ip
sudo iptables -I INPUT 1 --src $ip/24 -j ACCEPT
sudo iptables -t raw -I PREROUTING 1 --src $ip/24 -j NOTRACK

sudo ethtool -A $iface autoneg off rx off tx off

