#!/bin/bash

iface=$1

CPU=0
old=`pwd`
cd /sys/class/net/$iface/device/msi_irqs/
for irq in *; do
            echo $CPU > /proc/irq/$irq/smp_affinity_list
            CPU=$(($CPU +1 ))
done
cd $old
