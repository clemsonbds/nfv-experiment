#!/bin/bash

meanfile=comp_mean.txt
stddevfile=comp_stddev.txt
minfile=comp_min.txt
maxfile=comp_max.txt



envirs="native docker"
techs="direct macvlan sriov bridge"

echo "X $techs" > $meanfile
echo "X $techs" > $stddevfile
echo "X $techs" > $minfile
echo "X $techs" > $maxfile

for envir in $envirs; do
  printf "$envir " >> $meanfile
  printf "$envir " >> $stddevfile
  printf "$envir " >> $minfile
  printf "$envir " >> $maxfile

  for tech in $techs; do
    linecount=0
    meansum=0.0
    stddevsum=0.0
    minsum=0.0
    maxsum=0.0
    infile=${envir}-${tech}.txt
    echo "reading" $infile "..."
    while read line; do
      linecount=$(($linecount+1))
      mean=`echo $line|tr '=' ' '|awk '{print $4}'|rev|cut -c 3-|rev`
      stddev=`echo $line|tr '=' ' '|awk '{print $6}'|rev|cut -c 3-|rev`
      min=`echo $line|tr '=' ' '|awk '{print $8}'|rev|cut -c 3-|rev`
      max=`echo $line|tr '=' ' '|awk '{print $10}'|rev|cut -c 3-|rev`
#      echo $mean, $stddev, $min, $max
      meansum=`echo $meansum + $mean | bc`
      stddevsum=`echo $stddevsum + $stddev | bc`
      minsum=`echo $minsum + $min | bc`
      maxsum=`echo $maxsum + $max | bc`
    done <$infile
    echo $linecount, $meansum, $stddevsum, $minsum, $maxsum
#    mean=`echo $meansum / $linecount | bc -l`
    stddev=`echo $stddevsum / $linecount | bc -l`
    min=`echo $minsum / $linecount | bc -l`
    max=`echo $maxsum / $linecount | bc -l`
#    echo "$linecount",  $mean, $stddev, $min, $max
    printf "%0.3f " $mean >> $meanfile
    printf "%0.3f " $stddev >> $stddevfile
    printf "%0.3f " $min >> $minfile
    printf "%0.3f " $max >> $maxfile
  done

  printf "\n" >> $meanfile
  printf "\n" >> $stddevfile
  printf "\n" >> $minfile
  printf "\n" >> $maxfile
done
