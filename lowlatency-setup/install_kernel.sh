#!/bin/bash

kernel=3.13.0-57

apt-get update
apt-get install -y software-properties-common python-software-properties
add-apt-repository -y ppa:kernel-ppa/ppa
apt-get update
apt-get install -y linux-headers-$kernel linux-headers-$kernel-lowlatency linux-image-$kernel-lowlatency --fix-missing

printf "\nKernel $kernel installed, please reboot.\n"

