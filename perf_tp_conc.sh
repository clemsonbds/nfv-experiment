#!/bin/bash

server=$1
ncores=$2
size=$3

if [ -z "$size" ]; then
  echo "usage: $0 ncores packet_size"
  exit
fi

outfile=${0}.out
size=$(($size-42))
testlen=10

[ -e $outfile ] && rm $outfile

for core in `seq 1 $ncores`; do
  netperf -H $server -t UDP_STREAM -c -C -- -S 128M -m $size -l $testlen >> $outfile &
done

while [ ! -z "`ps --no-headers -C netperf`" ]; do
  sleep 1
done

succ=`cat $outfile | grep 212992 | wc -l`
packets_sent=`cat $outfile | grep 212992 | awk '{ s+=$4 } END { print s }'`
packets_recv=`cat $outfile | grep 425984 | awk '{ s+=$3 } END { print s }'`

[ $succ -ne $ncores ] && mv $outfile ${outfile}.$ncores.$size.error

[ -e $outfile ] && rm $outfile

echo "cmpl: $succ"
echo "spps: $(($packets_sent/$testlen))"
echo "rpps: $(($packets_recv/$testlen))"
