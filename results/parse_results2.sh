for cpu in 0 50 95; do
  f=blog_${cpu}_summary.txt
  rm $f
  for type in native macvlan sriov; do
    printf "$type " >> $f
    for x in `cat blog_${type}_${cpu}_rr.txt | grep 22 | awk '{print $6}'`; do
      printf "%s " $x >> $f
    done
    printf "\n" >> $f
  done
done
