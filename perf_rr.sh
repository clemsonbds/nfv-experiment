#!/bin/bash

target=$1
iterations=$2
length=$3
request_size=$4
response_size=$5
# do we care about local or remote socket buffer size?  maybe experiment to get lowest results first

[ -z "$target" ] && {
  echo "usage: $0 target iterations length request_size response_size"
  exit
}

[ -z "$iterations" ] && iterations=1
[ -z "$length" ] && length=10
[ -z "$request_size" ] && request_size=64
[ -z "$response_size" ] && response_size=$request_size

request_size=$(($request_size-42))
response_size=$(($response_size-42))

[ "$iterations" -gt 1 ] && iter_str="-i $iterations"

netperf -H $target -t UDP_RR $iter_str -l $length -- -r ${request_size},${response_size}
