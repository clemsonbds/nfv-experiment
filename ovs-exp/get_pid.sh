#!/bin/bash

GUESTNAME=$1

echo `docker inspect --format='{{ .State.Pid }}' "$GUESTNAME"`
