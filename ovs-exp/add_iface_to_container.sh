#!/bin/bash

GUEST_IFNAME=$1
CONTAINER_ID=$2
IPADDR=$3

[ -z "$IPADDR" ] && echo "usage: $0 ifname container_id ip_addr" && exit

CONTAINER_IFNAME=eth1
NSPID=`./get_pid.sh $CONTAINER_ID`

ip link set "$GUEST_IFNAME" netns "$NSPID"
ip netns exec "$NSPID" ip link set "$GUEST_IFNAME" name "$CONTAINER_IFNAME"
ip netns exec "$NSPID" ip addr add "$IPADDR" dev "$CONTAINER_IFNAME"
ip netns exec "$NSPID" ip link set "$CONTAINER_IFNAME" up
