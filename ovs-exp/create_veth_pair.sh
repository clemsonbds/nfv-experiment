#!/bin/bash

CONTAINER_ID=$1
NSPID=`./get_pid.sh $CONTAINER_ID`

LOCAL_IFNAME="vethpl${NSPID}"
GUEST_IFNAME="vethpg${NSPID}"

ip link add name "$LOCAL_IFNAME" type veth peer name "$GUEST_IFNAME"
ip link set "$LOCAL_IFNAME" up
