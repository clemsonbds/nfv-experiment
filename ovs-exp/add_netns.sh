#!/bin/bash

CONTAINER_ID=$1
NSPID=`./get_pid.sh $CONTAINER_ID`

[ ! -d /var/run/netns ] && mkdir -p /var/run/netns
rm -f "/var/run/netns/$NSPID"
ln -s "/proc/$NSPID/ns/net" "/var/run/netns/$NSPID"
