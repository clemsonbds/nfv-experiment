#!/bin/bash

nvnfs=$1

[ -z "$nvnfs" ] && {
  echo "usage: $0 num_vnfs"
  exit
}

# set up bridge somewhere else
bridge=br1

# set up an experiment
next_ip=10.10.1.2
#ping $next_ip -c 1
# expects an arp entry, set in advance with arp -s <ip> <mac>
next_mac=`arp ${next_ip}|tail -n 1|awk '{print $3}'`

sudo ovs-ofctl del-flows ${bridge}
port=`sudo ovs-ofctl show ${bridge} | grep eth1 | tr '(' ' ' | awk '{print $1}'`
sudo ovs-ofctl add-flow ${bridge} dl_dst=${next_mac},actions=output:${port}

for n in `seq 0 $(($nvnfs-1))`; do
	mac_n=$(($nvnfs - $n))
	mac=00:00:00:00:00:0${mac_n}
	ip_n=$(($mac_n+3))
	ip=10.10.1.${ip_n}

	echo "Starting VNF container with MAC $mac, redirecting to $next_mac"
	vnf=$(docker run -d -e "VNF_ID=${mac_n}" -e "VNF_INFILE=vnf_passthru.click" -e "VNF_WEB_SERVER=clemson.edu/~jwa2/click" -e "VNF_OUTPUT0=${next_ip}/${next_mac}" -e "VNF_IFACE_IN=eth1" rakurai/click-vnf ./run_click.sh)

	echo "Setting up container virtual interface on $bridge, with ip $ip at $mac"
	sudo pipework ${bridge} ${vnf} ${ip}/24 ${mac}

	ns_pid=`ip netns|head -n 1`

	port=`sudo ovs-ofctl show ${bridge} | grep $ns_pid | tr '(' ' ' | awk '{print $1}'`
	echo "Installing static flow dst MAC $mac on port $port"
	sudo ovs-ofctl add-flow ${bridge} dl_dst=${mac},actions=output:${port}

	echo "Dropping packets in kernel for namespace $ns_pid"
	sudo ip netns exec ${ns_pid} iptables -A INPUT -i eth1 -j DROP
	sudo ip netns exec ${ns_pid} iptables -A FORWARD -i eth1 -j DROP

	next_ip=${ip}
	next_mac=${mac}
done

