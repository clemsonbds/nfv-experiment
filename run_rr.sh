host=$1

shift
tests=$@
[ -z "$tests" ] && tests="64 128 256 512 1024 1280 1514"

iterations=10
length=10

for size in $tests; do
  ./perf_rr.sh $host $iterations $length $size 64
done
