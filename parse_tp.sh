#!/bin/bash

prefix=$1

for nchain in `seq 1 5`
do
  file=${prefix}_${nchain}_tp.txt
  printf "$nchain"

  cat $file | grep 212992 | while read line
  do
    [ -z `echo $line | awk '{print $6}'` ] || continue
    var=`echo $line | awk '{print $3}'`
    var=$(($var/10))
    printf " $var"
  done

  printf "\n"
done

