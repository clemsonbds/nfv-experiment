#!/bin/bash

sizes=(64 128 256 512 1024 1280 1514)

size_line=${sizes[*]}
echo "procs/size ${size_line}"

for proc in `seq 1 63`; do
  printf "%d" $proc

  for size in ${sizes[*]}; do
    sum=0

    for c in 4 20 36 52; do
      infile=conc_tcp_stream_${c}_${proc}_${size}.out
      [ -e "$infile" ] || continue
      tsum=`cat $infile | awk '{ sum+=$5} END {print sum}'`
      sum=$(echo $tsum + $sum | bc)
    done

    printf " %f" $sum
  done

  printf "\n"
done
