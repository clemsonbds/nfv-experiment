#!/bin/bash

run_id=$1
startnum=$2
nprocs=$3
size=$4

if [ -z "$size" ]; then
  echo "usage: $0 run_id start_ip num_procs packet_size"
  exit
fi

outfile=results/conc_tcp_stream_${startnum}_${run_id}_${size}.out
size=$(($size-42))
testlen=10
iterations=
#iterations="-i 10"

[ -e $outfile ] && rm $outfile

for proc in `seq 0 $(($nprocs-1))`; do
  ipnum=$(($startnum+$proc))
  [ $ipnum -gt 66 ] && break
  ip=10.10.1.$ipnum
#  >&2 echo "starting netperf to $ip"
  netperf -H $ip -t TCP_STREAM $iterations -c -C -P 0 -- -S 128M -m $size -l $testlen >> $outfile &
done

while [ ! -z "`ps --no-headers -C netperf`" ]; do
  sleep 1
done

#cat $outfile
#rm $outfile

#succ=`cat $outfile | grep 212992 | wc -l`
#packets_sent=`cat $outfile | grep 212992 | awk '{ s+=$4 } END { print s }'`
#packets_recv=`cat $outfile | grep 425984 | awk '{ s+=$3 } END { print s }'`

#[ $succ -ne $ncores ] && mv $outfile ${outfile}.$ncores.$size.error

#[ -e $outfile ] && rm $outfile

#echo "cmpl: $succ"
#echo "spps: $(($packets_sent/$testlen))"
#echo "rpps: $(($packets_recv/$testlen))"
