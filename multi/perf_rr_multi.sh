#!/bin/bash

conc=$1
iterations=$2
length=$3
req_size=$4
resp_size=$5

for s in `seq 1 $conc`; do
  ip=10.10.1.$(($s+3))
  ./perf_rr.sh $ip $iterations $length $req_size $resp_size &
done
