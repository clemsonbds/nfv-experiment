#!/bin/bash

max_procs=$1
[ -z "$max_procs" ] && {
  echo "usage: $0 max_procs"
  exit
}

# establish static arp entries for the 4 clients
# establish static arp entries for containers on 4 clients
clients=(10.10.1.100 10.10.1.101 10.10.1.102 10.10.1.103)
server=130.127.133.98

top_outfile=results/top_out.txt

for client in ${clients[*]}; do
  ssh $client "rm results/*"
done

rm $top_outfile

for total_procs in `seq 1 $max_procs`; do
  for size in 64 128 256 512 1024 1280 1514; do
    procs_rem=$total_procs

    for cnum in `seq 0 3`; do
      procs=1
      until [ $((${procs}*4)) -ge $total_procs ]; do procs=$((${procs}+1)); done
      [ $procs -gt $procs_rem ] && procs=$procs_rem

      [ $procs -le 0 ] && break

      procs_rem=$((${procs_rem}-${procs}))

      startnum=$(($cnum * 16 + 4))
      client_ip=${clients[${cnum}]}

      echo "starting netperf on $client_ip with args $startnum $procs $size"
      ssh ${client_ip} "nfv-experiment/multi/perf_tp_conc.sh $total_procs $startnum $procs $size" &
    done

    sleep 4
    top=`ssh 130.127.133.98 "top -b -n 1 | grep Cpu" | awk '{print $2, $4, $12}'`
    echo "$total_procs $size $top" >> $top_outfile

    echo "waiting for netperfs to finish"
    for client in ${clients[*]}; do
      nlist=`ssh $client "ps --no-headers -C netperf"`
      until [ -z "$nlist" ]; do
        sleep 1
        nlist=`ssh $client "ps --no-headers -C netperf"`
      done
    done

  done
done
