#!/bin/bash

for x in `seq 1 63`; do
  ip=10.10.1.$(($x+3))
  mac_n=`printf "%02d" $x`
  mac=00:00:00:00:00:$mac_n
#  echo $ip $mac
  sudo arp -s $ip $mac
done

