#!/bin/bash

port=eth1
bridge=br1
ip=10.10.1.3

sudo ovs-vsctl del-br $bridge
sudo ifconfig $port $ip
