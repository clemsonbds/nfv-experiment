#!/bin/bash

docker kill $(docker ps -q)

for ns in `ip netns`; do
  sudo ip netns del $ns
done

