#!/bin/bash

nvfns=$1

[ -z "$nvnfs" ] && {
  echo "usage: $0 num_vnfs"
  exit
}

iface=eth1

config_file=config.xen

# set up an experiment
next_ip=10.10.1.2
ping $next_ip -c 1
next_mac=`arp ${next_ip}|tail -n 1|awk '{print $3}'`

for n in `seq 0 $(($nvnfs-1))`; do
	mac_n=$(($nvnfs - $n))
	mac=00:00:00:00:00:0${mac_n}
	ip_n=$(($mac_n+3))
	ip=10.10.1.${ip_n}

	vnf_name="click$n"

	echo "kernel = './minios/build/clickos_x86_64'" > $config_file
	echo "vcpus = '1'" >> $config_file
	echo "memory = '100'" >> $config_file
	echo "vif = ['mac=$mac,bridge=xenbr0']" >> $config_file
	echo "name = '$vnf_name'" >> $config_file
	echo "on_crash = 'preserve'" >> $config_file


	VNF_IFACE_IN=0 \
	VNF_IFACE_OUT=0 \
	VNF_INFILE=vnf_passthru.click \
	VNF_OUTPUT0=${next_ip}/${next_mac} \
	vnf_config.sh > vnf.click

	# start ClickOS with $config_file and vnf.click

	echo "Starting ClickOS VM with MAC $mac, redirecting to $next_mac"
	# start VM here

	next_ip=${ip}
	next_mac=${mac}
done


