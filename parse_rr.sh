#!/bin/bash

prefix=$1

for nchain in `seq 1 5`
do
  file=${prefix}_${nchain}_rr.txt
  printf "$nchain"

  cat $file | grep 212992 | while read line
  do
    var=`echo $line | awk '{print $6}'`
    [ -z "$var" ] || printf " $var"
  done

  printf "\n"
done

