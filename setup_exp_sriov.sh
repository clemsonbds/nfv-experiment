#!/bin/bash

# steps for adding VFs to namespaces, don't need with pipework
#sudo ip netns add vnf_ns1                                         # add the namespace
#sudo ip netns exec vnf_ns1 ip link set lo up                      # bring up loopback
#sudo ip link set eth4 netns vnf_ns1                               # assign interface to namespace
#sudo ip netns exec vnf_ns1 ip link set dev eth4 name eth0         # change name for container use
#sudo ip netns exec vnf_ns1 ip addr add 10.10.1.3/24 dev eth0      # give it an ip address
#sudo ip netns exec vnf_ns1 ip link set eth0 up                    # bring it up

# set up an experiment
nvnfs=$1

[ -z "$nvnfs" ] && {
  echo "usage: $0 num_vnfs"
  exit
}

# get enough interfaces from the last NVFS ones named eth*
SRIOV_INTERFACES=`ifconfig -a |grep eth|grep -v veth|awk '{print $1}'|tail -n $nvnfs`
next_ip=10.10.1.2
next_mac=`arp ${next_ip}|tail -n 1|awk '{print $3}'`

for n in `seq 0 $(($nvnfs-1))`; do
	mac_n=$(($nvnfs - $n))
	mac=00:00:00:00:00:0${mac_n}
	ip_n=$(($mac_n+3))
	ip=10.10.1.${ip_n}

	iface=`echo $SRIOV_INTERFACES | awk -v col="$(($n+1))" '{print $col}'`

	echo "Starting VNF container redirecting to $next_ip at $next_mac"
	vnf=$(docker run -d -e "VNF_ID=${mac_n}" -e "VNF_INFILE=vnf_passthru.click" -e "VNF_WEB_SERVER=clemson.edu/~jwa2/click" -e "VNF_OUTPUT0=${next_ip}/${next_mac}" -e "VNF_IFACE_IN=eth1" rakurai/click-vnf ./run_click.sh)

	echo "Setting up container virtual interface on $iface, with ip $ip at $mac"
	sudo pipework ${iface} ${vnf} ${ip}/24 ${mac}

	ns_pid=`ip netns|head -n 1`
	echo "Dropping packets in kernel for namespace $ns_pid"
	sudo ip netns exec ${ns_pid} iptables -A INPUT -i eth1 -j DROP
	sudo ip netns exec ${ns_pid} iptables -A FORWARD -i eth1 -j DROP

	next_ip=${ip}
	next_mac=${mac}
done

