#!/bin/bash

#setup
bridge=eth1
server=$(docker run -d rakurai/netperf netserver -D)
client=$(docker run -d rakurai/netperf-exp sleep 10000)

sudo pipework $bridge $server 10.10.1.4/24
sudo pipework $bridge $client 10.10.1.5/24

#experiment
#for size in 64 128 256 512 1024 1280 1514; do
#size=64
#  docker exec -it $client bash -c "netperf -H 10.10.1.4 -t UDP_RR -- -m $size"
#done

#cleanup
#docker kill $server
#docker kill $client
#sudo ip link del $bridge
