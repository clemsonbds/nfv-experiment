#!/bin/bash

nvnfs=$1

[ -z "$nvnfs" ] && {
  echo "usage: $0 num_vnfs"
  exit
}

# set up bridge somewhere else
bridge=br1

# set up an experiment

#sudo ovs-ofctl del-flows ${bridge}
#port=`sudo ovs-ofctl show ${bridge} | grep eth1 | tr '(' ' ' | awk '{print $1}'`
#sudo ovs-ofctl add-flow ${bridge} dl_dst=${next_mac},actions=output:${port}

for n in `seq 1 $nvnfs`; do
	mac_n=`printf "%02d" $n`
	mac=00:00:00:00:00:${mac_n}
	ip_n=$(($n+3))
	ip=10.10.1.${ip_n}

	echo "Starting VNF container with MAC $mac, redirecting to $next_mac"
	vnf=$(docker run -d rakurai/netperf netserver -D)

	echo "Setting up container virtual interface on $bridge, with ip $ip at $mac"
	sudo pipework ${bridge} ${vnf} ${ip}/24 ${mac}

	ns_pid=`ip netns|head -n 1`

	port=`sudo ovs-ofctl show ${bridge} | grep $ns_pid | tr '(' ' ' | awk '{print $1}'`
	echo "Installing static flow dst MAC $mac on port $port"
	sudo ovs-ofctl add-flow ${bridge} dl_dst=${mac},actions=output:${port}

done

