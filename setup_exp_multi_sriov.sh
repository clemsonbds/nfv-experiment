#!/bin/bash

# set up an experiment
nvnfs=$1

[ -z "$nvnfs" ] && {
  echo "usage: $0 num_vnfs"
  exit
}

# get enough interfaces from the last NVFS ones named eth*
#SRIOV_INTERFACES=`ifconfig -a |grep eth|grep -v veth|awk '{print $1}'|tail -n $nvnfs`
#echo $SRIOV_INTERFACES
for n in `seq 1 $nvnfs`; do
	mac_n=`printf "%02d" $n`
	mac=00:00:00:00:00:$mac_n
	ip_n=$(($n+3))
	ip=10.10.1.${ip_n}

#	iface=`echo $SRIOV_INTERFACES | awk -v col="$n" '{print $col}'`
	iface=sriov$n
	vnf=$(docker run -d rakurai/netperf netserver -D)

	echo "Setting up container virtual interface on $iface, with ip $ip at $mac"
	sudo pipework ${iface} ${vnf} ${ip}/24 ${mac}

#	ns_pid=`ip netns|head -n 1`
#	echo "Dropping packets in kernel for namespace $ns_pid"
#	sudo ip netns exec ${ns_pid} iptables -A INPUT -i eth1 -j DROP
#	sudo ip netns exec ${ns_pid} iptables -A FORWARD -i eth1 -j DROP
done

